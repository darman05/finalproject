import React from 'react';
import { StyleSheet, View, Text} from 'react-native';
import { createStackNavigator} from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';

import WelcomeScreen from '../pages/WelcomeScreen';
import AboutScreen from '../pages/AboutScreen';
import RegisterScreen from '../pages/RegisterScreen';
import SkillProject from '../pages/SkillProject';
import HomeScreen from '../pages/Home';
import LoginScreen from '../pages/LoginScreen';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawwer = createDrawerNavigator();

export default function Router() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="WelcomeScreen" component={WelcomeScreen}/>
        <Stack.Screen name="LoginScreen" component={LoginScreen}/>
        <Stack.Screen name="HomeScreen" component={HomeScreen}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}
const MainApp=()=>(
    <Tab.Navigator>
      <Tab.Screen name="RegisterScreen" component={RegisterScreen}/>
      <Tab.Screen name="HomeScreen" component={HomeScreen}/>
      <Tab.Screen name="SkillProject" component={SkillProject}/>
    </Tab.Navigator>
  )
const MyDrawwer = ()=>(
  <Drawwer.Navigator>
    <Drawwer.Screen name="App" component={MainApp}/>
    <Drawwer.Screen name="AboutScreen" component={AboutScreen}/>
  </Drawwer.Navigator>
  )
