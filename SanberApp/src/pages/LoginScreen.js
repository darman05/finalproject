import React from 'react'
import {ScrollView,
        Platform,
        View,
        Text,
          Image,
          Button,
          StyleSheet,
          TextInput,
           TouchableOpacity,
           KeyboardAvoidingView,
           TouchableOpacityBase} from 'react-native'

const LoginScreen = ({navigation}) => {
    return (
        <KeyboardAvoidingView
        behavior={Platform.OS =="ios" ? "padding" : "height"}
        style ={styles.container}
        >
        <ScrollView>
           <View style ={styles.container}>
              <Image
              style={{ height: 100, width: 120, marginLeft: 200, marginTop:20 }}
              source={require('../assets/image1.png')}/>
              <Text style={styles.welcometext}>Welcome Back</Text> 
              <View style={styles.forminput}>
                 <Text style={styles.formtext}>Email/Username</Text>
                 <TextInput style={styles.input} 
                 placeholder="Email@gmail.com"/>
              </View>    
        <View style ={styles.forminput}>
         <Text style ={styles.formtext}>Password</Text>
         <TextInput style={styles.input}
         placeholder="Masukan Password"
         secureTextEntry={true} />
        </View>
        <View style ={styles.kotaklogin}>
            <TouchableOpacity style={styles.btlogin}>
             <Text style={styles.logintext2}>Login</Text>
            </TouchableOpacity>
        </View>

       </View> 
    </ScrollView> 
    </KeyboardAvoidingView>
    
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    container:{
        flex:1,
    }, logintext:{
        fontSize:24,
        marginTop:63,
        textAlign: 'center',
        color:'#003366',
        marginVertical:10
    },
    logintext2:{
        fontSize:20,
        color:'#003366',
        textAlign : "center"
        
    },
    formtext:{
        color:"#003366"
    },
     autotext:{
        fontSize:20,
        color:'#3EC6FF'
    },
    forminput:{
        marginHorizontal: 30,
        marginVertical: 5,
        alignContent:'center',
        width:294
    },
    formlogin:{
        marginHorizontal: 30,
        marginVertical: 105,
        alignContent:'center',
        width:294
    },
    input:{
        height: 40,
        borderColor: "#003366",
        padding:10,
        borderWidth:1
    },
    welcometext:{
        fontSize:30,
        marginTop:63,
        textAlign: 'center',
        color:'#003366',
        marginVertical:20,
        fontWeight: "bold"
    },
    btlogin:{
        alignContent:"center",
        alignItems:'center',
        backgroundColor:'#3EC6FF',
        padding:10,
        borderRadius: 20,
        marginHorizontal:30,
        marginBottom:10,
        width: 140,
        
    }     
})
