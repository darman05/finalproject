import React from "react"
import { StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    Button
} from "react-native"

const AboutScreen = () => {
    return (
    <ScrollView>
        <View style={styles.container}>
            <Text style={styles.judul}>Tentang Saya</Text>
            <fontAwesome5 
            name='user-circle'
            size={200} solid
            color="#EFEFEF"
            style={styles.icon}
             />
            <Text style={styles.judul}>Darman Sitepu</Text>
            <Text style={styles.kerjaan}>Web Depelover</Text>
            <View style={styles.kotak}>
                <Text style={styles.juduldalam}>Portofolio</Text>
                <View style={styles.kotakdalam}>
                    <View>
                      <fontAwesome5 
                      name="gitlab"
                      color="#3EC6FF"
                      size={40}
                      style={styles.icon}/>
                      <Text style={styles.textdalam}>@Darman05</Text>
                    </View>
                    <View>
                      <fontAwesome5 name="github" color="#3EC6FF" size={40} style={styles.icon}/>
                      <Text style={styles.textdalam}>@darman</Text>
                    </View>
                </View>
            </View>
            <View style={styles.kotak}>
               <Text style={styles.juduldalam}>Kontak Saya</Text>
               <View style={styles.kotakdalamver}>
                   <View style={styles.kotakdalamverhub}>
                     <View>
                        <fontAwesome5 name="facebook" size={40} style={styles.icon}/>
                     </View>
                     <View style={styles.textname}>
                        <Text style={styles.textdalam}>@darman</Text>
                     </View>
                   </View>
                   <View style={styles.kotakdalamverhub}>
                        <View>
                           <fontAwesome5 name="instagram" size={40} style={styles.icon}/>
                        </View>
                        <View style={styles.textname}>
                          <Text style={styles.textdalam}>@darman</Text>
                        </View>
                   </View>
                   <View style={styles.kotakdalamverhub}>
                       <View>
                         <fontAwesome5 name="twitter" size={40} style={styles.icon}/>
                       </View>
                       <View style={styles.textname}>
                         <Text style={styles.textdalam}>@darman</Text>
                       </View>
                   </View>    
               </View>
           </View>
       </View>
    </ScrollView>
    )
}

export default AboutScreen

const styles = StyleSheet.create({
    container:{
        marginTop:64
    },
    judul:{
        fontSize:36,
        fontWeight:"bold",
        color:'#003366',
        textAlign:'center'
    },
    icon:{
        textAlign:'center'
    },
    name:{
        fontSize:24,
        fontWeight:"bold",
        color:'#003366',
        textAlign:'center'
    },
    kerjaan:{
        fontSize:16,
        fontWeight:"bold",
        color:'#3EC6FF',
        textAlign:'center',
        marginBottom:7
    },
    kotak:{
        borderColor:"blue",
        borderRadius:10,
        borderBottomColor:"#000",
        padding: 5,
        backgroundColor:"#EFEFEF",
        marginBottom:9
    },
    kotakdalam:{
        borderTopWidth:2,
        borderTopColor:"#003366",
        flexDirection:"row",
        justifyContent:"space-around"
    },
    kotakdalamver:{
        borderTopWidth: 2,
        borderTopColor:"#003366",
        flexDirection:"column",
        justifyContent:"space-around"
    },
    kotakdalamverhub:{
        height:50,
        flexDirection:"row",
        justifyContent:"center",
        marginBottom: 2
    },
    juduldalam:{
        fontSize:18,
        color:"#003366"
    },
    textdalam:{
        fontSize:16,
        fontWeight:"bold",
        color:"#003366",
    textAlign:'center'
  },
  input:{
      height:40,
      borderColor:"grey",
      borderWidth:1
  },
  textname:{
      justifyContent:"center",
      marginLeft:10
  }
})
