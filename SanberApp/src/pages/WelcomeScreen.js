import React from 'react'
import {ScrollView,
        Platform,
        View,
        Text,
          Image,
          Button,
          StyleSheet,
          TextInput,
           TouchableOpacity,
           KeyboardAvoidingView,
           TouchableOpacityBase} from 'react-native'

const WelcomeScreen = ({navigation}) => {
    return (
        <KeyboardAvoidingView
        behavior={Platform.OS =="ios" ? "padding" : "height"}
        style ={styles.container}
        >
        <ScrollView>
           <View style ={styles.container}>
              <Image
              style={{ height: 100, width: 120, marginLeft: 200, marginTop:20 }}
              source={require('../assets/image1.png')}/>
              <Text style={styles.welcometext}>Welcome To MyStore</Text>
               
        
        <View style ={styles.forminput2}>
            <TouchableOpacity style={styles.btlogin}>
             <Text style={styles.logintext2}>Register</Text>
            </TouchableOpacity>
        </View>
        <View style ={styles.forminput2}>
            <TouchableOpacity style={styles.btlogin2}>
             <Text style={styles.logintext2}>Login</Text>
            </TouchableOpacity>
        </View>
       </View> 
    </ScrollView> 
    </KeyboardAvoidingView>
    )
}

export default WelcomeScreen

const styles = StyleSheet.create({
    container:{
        flex:1,
    }, logintext:{
        fontSize:24,
        marginTop:63,
        textAlign: 'center',
        color:'#003366',
        marginVertical:10
    },
    logintext2:{
        fontSize:24,
        color:'#003366',
        textAlign : "center"

    },
    formtext:{
        color:"#003366"
    },
     autotext:{
        fontSize:20,
        color:'#3EC6FF'
    },
   
    forminput:{
        marginHorizontal: 30,
        marginVertical: 5,
        alignContent:'center',
        width:242,
        color:'#3EC6FF'
    },
    forminput2:{
        marginHorizontal: 30,
        marginVertical: 5,
        alignContent:'center',
        width:242,
        color:'#3EC6FF'
    },
    formlogin:{
        marginHorizontal: 40,
        marginVertical: 200,
        alignContent:'center',
        width:100
    },
    input:{
        height: 40,
        borderColor: "#003366",
        padding:10,
        borderWidth:1
    },
    welcometext:{
        fontSize:30,
        marginTop:63,
        textAlign: 'center',
        color:'#003366',
        marginVertical:20,
        fontWeight: "bold"
    },
    btlogin:{
        alignItems:'center',
        backgroundColor:'#3EC6FF',
        padding:10,
        borderRadius: 16,
        marginHorizontal:30,
        marginBottom:10,
        width: 240,
    },
    btlogin2:{
        alignItems:'center',
        backgroundColor: "#A9A9A9",
        padding:10,
        borderRadius: 16,
        marginHorizontal:30,
        marginBottom:10,
        width: 240,
    }          
})
